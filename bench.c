/* SPDX-License-Identifier: MIT */
/* Copyright 2023-2025 Alexander Zuepke */
/*
 * bench.c
 *
 * Benchmark memory performance
 *
 * Compile:
 *   $ gcc -O2 -DNDEBUG -W -Wall -Wextra -Werror bench.c -o bench
 *
 * Run (as root)
 *   $ ./bench <read|write|modify> [-s <size-in-MiB>] [-c <cpu>] [-p <prio>]
 *
 * azuepke, 2023-02-22: standalone version
 * azuepke, 2023-12-07: explicit prefetches
 * azuepke, 2023-12-12: more prefetches
 * azuepke, 2023-12-19: dump regulation-related PMU counters
 * azuepke, 2023-12-20: argument parsing
 * azuepke, 2023-12-24: incorporate worst-case memory access benchmarks
 * azuepke, 2023-12-25: test automation and CSV files
 * azuepke, 2024-03-28: RISC-V version
 * azuepke, 2025-02-18: pointer chasing
 * azuepke, 2025-02-19: sizes in KiB
 * azuepke, 2025-02-25: configurable PMU counters
 * azuepke, 2025-02-26: PMU event sources
 */

#define _GNU_SOURCE	/* for sched_setaffinity() */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <linux/mman.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sched.h>
#include <linux/perf_event.h>
#include <unistd.h>
#include <asm/unistd.h>
#include <errno.h>
#include <assert.h>


/* program name */
#define PROGNAME "bench"

/* build ID, passed as string to compiler via -DBUILDID=<...> */
#ifndef BUILDID
#define BUILDID "local"
#endif

////////////////////////////////////////////////////////////////////////////////

/* hardcoded size of a cacheline */
#define CACHELINE_SIZE 64

#if defined __aarch64__
/* default mapping size in MiB */
#define DEFAULT_MB 8
#elif defined __riscv
/* default mapping size */
#define DEFAULT_MB 1
#elif defined __x86_64__
/* default mapping size */
#define DEFAULT_MB 16
#else
#errir unknown system
#endif

/* test mapping */
static char *map_addr;
static char *map_addr_end;
static size_t map_size;
/* ==0: small pages, 1: huge pages */
static int map_huge = 0;

/* global options */
static int option_perf = 0;
static int option_huge = 0;
static int option_step = 0;
/* ==0: infinite, >0: limited number of loops */
static int option_num_loops = 0;
/* print delay milliseconds, default 1s */
static int option_print_delay_ms = 1000;
static int option_all = 0;
static FILE *csv_file = NULL;
static int option_csv_no_header = 0;

static int pinned_cpu_id = -1;

////////////////////////////////////////////////////////////////////////////////

static int atoi_robust(const char *s)
{
	char *end;
	long val;

	val = strtol(s, &end, 0);
	if ((end == s) || (*end != '\0')) {
		fprintf(stderr, "error: invalid numeric parameter: %s\n", s);
		exit(EXIT_FAILURE);
	}

	return val;
}

////////////////////////////////////////////////////////////////////////////////

/* up to sixteen perf counters */
#define NUM_PERF 16

/* actual number of perf counters (auto detected) */
static unsigned int num_perf;

/* perf counter configuration */
struct perf_config {
	const char *type_name;
	unsigned int type;
	unsigned long long config;
};
static struct perf_config perf_config[NUM_PERF] = {
#if defined __aarch64__
	/* for Cortex-A53, A57, A72 */
	{ .config = 0x0017, },	/* L2D refill */
	{ .config = 0x0018, },	/* L2D write-back */
#elif defined __riscv
	/* none */
#elif defined __x86_64__
	/* for Intel */
	{ .config = 0x412e, },	/* LONGEST_LAT_CACHE.MISS */
#endif
};

/* fds of perf */
static int perf_fds[NUM_PERF];

/* previous and current values and deltas */
static unsigned long long perf_prev_values[NUM_PERF];
static unsigned long long perf_curr_values[NUM_PERF];
static unsigned long long perf_delta_values[NUM_PERF];
static double perf_factor[NUM_PERF];


/* perf_event_open() system call, not exported in libc */
static inline int perf_event_open(struct perf_event_attr *attr, pid_t pid, int cpu, int group_fd, unsigned long flags)
{
	return syscall(__NR_perf_event_open, attr, pid, cpu, group_fd, flags);
}

/* retrieve PMU type for struct perf_event_attr::type field
 * PMUs like uncore get type IDs beyond the predefined numbers.
 */
static inline int perf_pmu_type(const char *name)
{
	char buf[80];
	ssize_t r;
	int fd;

	snprintf(buf, sizeof(buf), "/sys/bus/event_source/devices/%s/type", name);

	fd = open(buf, O_RDONLY);
	if (fd == -1) {
		return -1;
	}

	r = read(fd, buf, sizeof(buf) - 1);
	close(fd);

	if (r <= 0) {
		return -1;
	}
	buf[r] = '\0';

	return atoi_robust(buf);
}

/* configure perf counters:
 *
 * the configuration string passed with --perf-config contains one or more
 * comma-separated PMU counters in 64-bit hexadecimal notation,
 * followed by an optional PMU type string separated by "@",
 * followed by an optional factor separated by "*" or "/" and a float.
 * With "*" the factor is multiplied, with "/" the factor is divided.
 *
 * Format:
 *   <pmc[@type[<*|/>factor]][,...]]>
 *
 * Examples:
 *
 * Configure PMCs 0x17 and 0x18
 *   --perf-config 0x17,0x18
 *
 * Configure PMCs 0x17 and 0x1a, but apply a factor of two to PMC 0x1a:
 *   --perf-config 0x17,0x1a*2
 *
 * Configure PMCs 0x13 and 0x1e, but apply a factor of 0.5 to PMC 0x13:
 *   --perf-config 0x13*0.5,0x1a
 *
 * Configure PMC 0x19 using the arm_dsu_pmu: and a factor of 0.25:
 *   --perf-config 0x19@arm_dsu_pmu*0.25
 *   --perf-config 0x19@arm_dsu_pmu/4
 */
static void perf_configure(const char *perf_config_str)
{
	unsigned long long config;
	const char *pmu_type_name;
	const char *start;
	int reciprocal;
	double factor;
	int pmu_type;
	char *end;

	for (unsigned int i = 0; i < NUM_PERF; i++) {
		perf_factor[i] = 1.0;
	}

	if (perf_config_str == NULL) {
		return;
	}

	for (unsigned int i = 0; i < NUM_PERF; i++) {
		perf_config[i].config = 0;
	}

	start = perf_config_str;
next:
	config = strtoull(start, &end, 16);
	if (end == start) {
		fprintf(stderr, "error: invalid PMC counter: %s\n", start);
		exit(EXIT_FAILURE);
	}

	pmu_type = 0;
	pmu_type_name = NULL;
	if (*end == '@') {
		end++;
		start = end;
		while (*end != '\0' && *end != '*' && *end != '/' && *end != ',') {
			end++;
		}
		if (end == start) {
			fprintf(stderr, "error: invalid PMC type: %s\n", start);
			exit(EXIT_FAILURE);
		}
		pmu_type_name = strndup(start, end - start);
		if (pmu_type_name == NULL) {
			perror("strndup");
			exit(EXIT_FAILURE);
		}
		pmu_type = perf_pmu_type(pmu_type_name);
		if (pmu_type < 0) {
			fprintf(stderr, "# perf: unknown PMC type: %s\n", pmu_type_name);
			if (strcmp(pmu_type_name, "amd_l3") == 0) {
				fprintf(stderr, "# perf: maybe a kernel module is missing: sudo modprobe amd-uncore\n");
			}
			if (strncmp(pmu_type_name, "arm_dsu", 7) == 0) {
				fprintf(stderr, "# perf: maybe a kernel module is missing: sudo modprobe arm_dsu_pmu\n");
			}
		}
	}

	factor = 1;
	if (*end == '*' || *end == '/') {
		reciprocal = (*end == '/');
		end++;
		start = end;
		factor = strtod(start, &end);
		if (end == start) {
			fprintf(stderr, "error: invalid PMC factor: %s\n", start);
			exit(EXIT_FAILURE);
		}
		if (reciprocal) {
			factor = 1.0 / factor;
		}
	}

	if (*end != ',' && *end != '\0') {
		fprintf(stderr, "error: invalid PMC config: %s\n", end);
		exit(EXIT_FAILURE);
	}

	if (num_perf >= NUM_PERF) {
		fprintf(stderr, "error: too many PMC configurations, limit %d\n", NUM_PERF);
		exit(EXIT_FAILURE);
	}
	perf_config[num_perf].type_name = pmu_type_name;
	perf_config[num_perf].type = pmu_type;
	perf_config[num_perf].config = config;
	perf_factor[num_perf] = factor;
	num_perf++;

	if (*end == ',') {
		end++;
		start = end;
		goto next;
	}
}

static int perf_open(void)
{
	struct perf_event_attr attr = { 0 };
	unsigned long flags;
	unsigned int cpu;
	int group_fd;
	pid_t pid;
	int ctr;
	int err;
	int fd;

	attr.size = sizeof(attr);
	attr.config = 0;	/* see below */
	attr.read_format = PERF_FORMAT_GROUP;
	attr.disabled = 0; /* initially enabled */
	//attr.pinned = 1;	/* pinned to PMU -> EINVAL */
	//attr.exclusive = 1; /* exclusive use of PMU by this process -> EINVAL */

	group_fd = -1;	/* first in group */
	flags = PERF_FLAG_FD_CLOEXEC;

	for (ctr = 0; ctr < NUM_PERF; ctr++) {
		attr.type = perf_config[ctr].type;
		attr.config = perf_config[ctr].config;
		if (attr.config == 0) {
			break;
		}
		if (attr.type == 0) {
			/* normal per-core PMU counter*/
			attr.type = PERF_TYPE_RAW;
			pid = 0;				/* current process */
			cpu = -1;				/* any CPU */
		} else {
			/* global / uncore PMU counters require pinning to specific cores */
			pid = -1;				/* any process */
			cpu = pinned_cpu_id;	/* this CPU */
		}

		fd = perf_event_open(&attr, pid, cpu, group_fd, flags);
		if (fd == -1) {
			err = errno;
			if (err == EACCES) {
				printf("# perf tracing requires root permissions, rerun as root user\n");
				num_perf = 0;
			} else if (err == ENODEV) {
				printf("# perf tracing does not support tracing of hardware counters\n");
			} else {
				fprintf(stderr, "# perf tracing: error setting up perf counter %d type %u config 0x%llx: ", ctr, attr.type, attr.config);
				errno = err;
				perror("perf_event_open");
				if (pid == -1) {
					fprintf(stderr, "# perf tracing: consider to pin the benchmark to a specific core\n");
				}
			}
			return err;
		}

		perf_fds[ctr] = fd;
	}
	num_perf = ctr;
	return 0;
}

static inline unsigned long long perf_read_one(int fd)
{
	unsigned long long buf[2];
	ssize_t r;

	if (fd == 0) {
		/* unconfigured fd, stdin is not a valid PMC */
		return 0;
	}

	/* We except to read one more 64-bit values than counters,
	 * the first value is the number of counters.
	 */
	r = read(fd, buf, sizeof(buf));
	if (r == -1) {
		perror("perf_read");
		exit(EXIT_FAILURE);
	}
	if (r != sizeof(buf)) {
		fprintf(stderr, "perf_trace: read %zd, expected %zd\n", r, sizeof(buf));
		exit(EXIT_FAILURE);
	}
	assert(buf[0] == 1);
	return buf[1];
}

static void perf_read(unsigned long long *values)
{
	for (unsigned int i = 0; i < num_perf; i++) {
		values[i] = perf_read_one(perf_fds[i]);
	}
}

static inline unsigned long long *perf_delta(const unsigned long long *curr_values, const unsigned long long *prev_values, unsigned long long *delta_values)
{
	for (unsigned int i = 0; i < num_perf; i++) {
		delta_values[i] = curr_values[i] - prev_values[i];
	}

	return delta_values;
}

////////////////////////////////////////////////////////////////////////////////

/* return ts (in nanoseconds, risk of overflow) */
static inline unsigned long long timespec_ns(const struct timespec *ts)
{
	unsigned long long val;

	assert(ts != NULL);

	val = ts->tv_sec * 1000000000ull;
	val += ts->tv_nsec;

	return val;
}

/* ts += delta_ns */
static inline struct timespec *timespec_inc_by(struct timespec *ts, unsigned long long delta_ns)
{
	assert(ts != NULL);
	assert(ts->tv_sec >= 0);
	assert(ts->tv_nsec >= 0);
	assert(ts->tv_nsec < 1000000000);

	ts->tv_sec += delta_ns / 1000000000ull;
	ts->tv_nsec += delta_ns % 1000000000ull;
	if (ts->tv_nsec >= 1000000000) {
		ts->tv_nsec -= 1000000000;
		ts->tv_sec += 1;
	}

	assert(ts->tv_nsec >= 0);
	assert(ts->tv_nsec <= 999999999);

	return ts;
}

/* ts_out = ts_a - ts_b */
static inline struct timespec *timespec_sub(const struct timespec *ts_a, const struct timespec *ts_b, struct timespec *ts_out)
{
	assert(ts_a != NULL);
	assert(ts_a->tv_sec >= 0);
	assert(ts_a->tv_nsec >= 0);
	assert(ts_a->tv_nsec < 1000000000);
	assert(ts_b != NULL);
	assert(ts_b->tv_sec >= 0);
	assert(ts_b->tv_nsec >= 0);
	assert(ts_b->tv_nsec < 1000000000);
	assert(ts_out != NULL);

	ts_out->tv_sec = ts_a->tv_sec - ts_b->tv_sec;
	ts_out->tv_nsec = ts_a->tv_nsec - ts_b->tv_nsec;
	if (ts_out->tv_nsec < 0) {
		ts_out->tv_nsec += 1000000000;
		ts_out->tv_sec -= 1;
	}

	assert(ts_out->tv_sec >= 0);
	assert(ts_out->tv_nsec >= 0);
	assert(ts_out->tv_nsec <= 999999999);

	return ts_out;
}

/* test if ts_a is less than or equal to ts_b */
static inline int timespec_is_le(const struct timespec *ts_a, const struct timespec *ts_b)
{
	assert(ts_a != NULL);
	assert(ts_a->tv_sec >= 0);
	assert(ts_a->tv_nsec >= 0);
	assert(ts_a->tv_nsec < 1000000000);
	assert(ts_b != NULL);
	assert(ts_b->tv_sec >= 0);
	assert(ts_b->tv_nsec >= 0);
	assert(ts_b->tv_nsec < 1000000000);

	return ((ts_a->tv_sec < ts_b->tv_sec) ||
	        ((ts_a->tv_sec == ts_b->tv_sec) && (ts_a->tv_nsec <= ts_b->tv_nsec)));
}

////////////////////////////////////////////////////////////////////////////////

/* conversion from MB/s to MiB/s */
static inline double to_mib(double val)
{
	return val * (1000 * 1000) / (1024 * 1024);
}

////////////////////////////////////////////////////////////////////////////////

static inline unsigned int next_highest_power_of_2(unsigned int val)
{
	val--;
	val |= val >> 1;
	val |= val >> 2;
	val |= val >> 4;
	val |= val >> 8;
	val |= val >> 16;
	val++;

	return val;
}

static inline unsigned int ilog2(unsigned long val)
{
	unsigned int max_bits = (sizeof(val) * 8) - 1;
	return max_bits - __builtin_clzl(val);
}

////////////////////////////////////////////////////////////////////////////////

/* LFSR repeating after 2^n-1 steps */

/*
 * Roy Ward, Timothy C.A. Molteno: "Table of Linear Feedback Shift Registers",
 * Electronics Technical Report No. 2012-1, University of Otago, NZ
 * https://www.physics.otago.ac.nz/reports/electronics/ETR2012-1.pdf
 */
static const unsigned int lfsr_tap_bits[] = {
	0x00000000,
	0x00000001,
	0x00000003,
	0x00000006,
	0x0000000c,
	0x00000014,
	0x00000030,
	0x00000060,
	0x000000b8,
	0x00000110,
	0x00000240,
	0x00000500,
	0x00000e08,
	0x00001c80,
	0x00003802,
	0x00006000,
	0x0000d008,
	0x00012000,
	0x00020400,
	0x00072000,
	0x00090000,
	0x00140000,
	0x00300000,
	0x00420000,
	0x00e10000,
	0x00d80000,
	0x01200000,
	0x03880000,
	0x07200000,
	0x09000000,
	0x14000000,
	0x32800000,
	0x48000000,
	0xa3000000,
};

/* LFSR step function, do not need with zero value */
static inline unsigned int lfsr(unsigned int x, unsigned int bits)
{
	unsigned int tap_bits;
	unsigned int y;
	unsigned int i;

	assert(x > 0);
	assert(bits <= 32);

	tap_bits = lfsr_tap_bits[bits];

	y = 0;
	for (i = 0; i < bits; i++) {
		if ((tap_bits & (1u << i)) != 0) {
			y ^= (x >> i) & 1;
		}
	}

	x = (x << 1) | y;
	x &= ((((1u << (bits - 1)) -1) << 1) | 1u);
	return x;
}

////////////////////////////////////////////////////////////////////////////////

struct cacheline {
	struct cacheline *next;
	unsigned long scratch;
	char padding[CACHELINE_SIZE - 2*sizeof(long)];
};

/* prepare a random pointer-chasing pattern based on an LFSR
 *
 * The LFSR generates unique values in the range 1..2^n-1
 * and repeats after 2^n-1 steps.
 * We start at array index zero and prepare pointers to the next value.
 * We let the last 2^n entry point to index 0 to close the loop.
 *
 * This works well for any power-of-two sizes.
 * For smaller sizes, we use an LFSR with the next higher power-of-two value.
 * If an generated index is beyond the array size, we continue generating values
 * until we're back in the valid range.
 */
static void prepare_chase_random(void *start, size_t size)
{
	struct cacheline *ptr = start;
	unsigned int lfsr_state;
	unsigned int lfsr_end;
	unsigned int bits;
	unsigned int next;
	unsigned int end;
	unsigned int pos;
	unsigned int i;

	end = size / CACHELINE_SIZE;
	lfsr_end = next_highest_power_of_2(end);
	bits = ilog2(lfsr_end);
	lfsr_state = -1;

	pos = 0;
	for (i = 0; i < lfsr_end-1; i++) {
		lfsr_state = lfsr(lfsr_state, bits);
		next = lfsr_state;
		if (next >= end) {
			continue;
		}
		ptr[pos].next = &ptr[next];
		pos = next;
	}
	ptr[pos].next = &ptr[0];
}

////////////////////////////////////////////////////////////////////////////////

/* flush cacheline, without any fences */
static inline void flush_cacheline(void *addr)
{
#if defined __aarch64__
	__asm__ volatile ("dc CIVAC, %0" : : "r"(addr) : "memory");
#elif defined __riscv
	/* FIXME: RISC-V has nothing usable yet */
	(void)addr;
#elif defined __x86_64__
	__asm__ volatile ("clflush %0" : : "m" (*(long*)addr) : "memory");
#endif
}

static inline void memory_barrier(void)
{
#if defined __aarch64__
	__asm__ volatile ("dmb sy" : : : "memory");
#elif defined __riscv
	__asm__ volatile ("fence rw,rw" : : : "memory");
#elif defined __x86_64__
	__asm__ volatile ("mfence" : : : "memory");
#endif
}

static inline void flush_cacheline_all(void)
{
	char *ptr = map_addr;
	const char *end = map_addr_end;

	for (; ptr < end; ptr += CACHELINE_SIZE) {
		flush_cacheline(ptr);
	}
	memory_barrier();
}

////////////////////////////////////////////////////////////////////////////////

/* generate a set of non-inlined benchmark loops */
#define BENCH_CACHELINE(name)	\
	static void name ## _linear(void)	\
	{	\
		char *ptr = map_addr;	\
		char *end = map_addr_end;	\
		for (; ptr < end; ptr += CACHELINE_SIZE) {	\
			name(ptr);	\
		}	\
	}	\
	static void name ## _step(size_t step)	\
	{	\
		char *ptr = map_addr;	\
		char *end = map_addr_end;	\
		for (; ptr < end; ptr += step) {	\
			name(ptr);	\
		}	\
	}

#define BENCH_NAMES(name)	\
	.bench_linear = name ## _linear,	\
	.bench_step = name ## _step

////////////////////////////////////////////////////////////////////////////////

/* read a cache line using a load of one word */
static inline void read_cacheline(void *addr)
{
	unsigned long *p = addr;
	unsigned long tmp;
	/* the compiler barriers prevent reordering */
	__asm__ volatile ("" : : : "memory");
	tmp = p[0];
	/* consume tmp */
	__asm__ volatile ("" : : "r"(tmp) : "memory");
}

BENCH_CACHELINE(read_cacheline)

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__

/* load a complete cache line using LDNP instructions */
static inline void ldnp_cacheline(void *addr)
{
	unsigned long long tmp1, tmp2;

	__asm__ volatile (
		"ldnp %0, %1, [%2, #0]\n"
		: "=&r"(tmp1), "=&r"(tmp2) : "r"(addr) : "memory");
}

BENCH_CACHELINE(ldnp_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

/* write to a complete cache line using stores */
static inline void write_cacheline(void *addr)
{
#if defined __aarch64__ || defined __x86_64__
	__uint128_t *p = addr;
	p[0] = 0;
	p[1] = 0;
	p[2] = 0;
	p[3] = 0;
#else
	unsigned long long *p = addr;
	p[0] = 0;
	p[1] = 0;
	p[2] = 0;
	p[3] = 0;
#if CACHELINE_SIZE > 32
	p[4] = 0;
	p[5] = 0;
	p[6] = 0;
	p[7] = 0;
#endif
#endif
}

BENCH_CACHELINE(write_cacheline)

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__

/* write to a complete cache line using DC ZVA instructions */
static inline void dczva_cacheline(void *addr)
{
	__asm__ volatile ("dc zva, %0" : : "r"(addr) : "memory");
}

BENCH_CACHELINE(dczva_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__

/* write to a complete cache line using STNP instructions */
static inline void stnp_cacheline(void *addr)
{
	__asm__ volatile (
		"stnp x0, x1, [%0, #0]\n"
		"stnp x2, x3, [%0, #16]\n"
		"stnp x4, x5, [%0, #32]\n"
		"stnp x6, x7, [%0, #48]\n"
		: : "r"(addr) : "memory");
}

BENCH_CACHELINE(stnp_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

/* modify a single word in a cache line, effectively a read-modify-write */
static inline void modify_cacheline(void *addr)
{
	unsigned long *p = addr;

	/* the compiler barriers prevent reordering */
	__asm__ volatile ("" : : : "memory");
	p[0] = (unsigned long) addr;
	__asm__ volatile ("" : : : "memory");
}

BENCH_CACHELINE(modify_cacheline)

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__
/* modify a single word in a cache line, effectively a read-modify-write */
static inline void prefetch_modify_cacheline(void *addr)
{
	unsigned long *p = addr;

	/* a look-ahead of 3 cache lines yields the highest performance on A53 */
	/* prefetch cacheline to L1 cache */
	__asm__ volatile ("prfm pstl1keep, [%0]" : : "r"(p + 3*CACHELINE_SIZE) : "memory");

	/* the compiler barriers prevent reordering */
	__asm__ volatile ("" : : : "memory");
	p[0] = (unsigned long) addr;
	__asm__ volatile ("" : : : "memory");
}

BENCH_CACHELINE(prefetch_modify_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__

/* write to a part of a cache line using STNP instructions */
static inline void stnp_modify_cacheline(void *addr)
{
	__asm__ volatile (
		"stnp x0, x1, [%0, #0]\n"
		: : "r"(addr) : "memory");
}

BENCH_CACHELINE(stnp_modify_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__ || defined __x86_64__

/* prefetch a full cacheline to L1 for reading */
static inline void prefetch_l1_cacheline(void *addr)
{
#if defined __aarch64__
	/* prefetch cacheline to L1 cache */
	__asm__ volatile ("prfm pldl1keep, [%0]" : : "r"(addr) : "memory");
#else
	/* prefetch cacheline to L1/L2/L3 cache */
	__asm__ volatile ("prefetcht0 %0" : : "m"(*(char*)addr) : "memory");
#endif
}

BENCH_CACHELINE(prefetch_l1_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__ || defined __x86_64__

/* prefetch a full cacheline to L1 for writing */
static inline void prefetch_l1w_cacheline(void *addr)
{
#if defined __aarch64__
	/* prefetch cacheline to L1 cache for store */
	__asm__ volatile ("prfm pstl1keep, [%0]" : : "r"(addr) : "memory");
#else
	/* prefetch cacheline to L1/L2/L3 cache for store */
	__asm__ volatile ("prefetchw %0" : : "m"(*(char*)addr) : "memory");
#endif
}

BENCH_CACHELINE(prefetch_l1w_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__

/* prefetch a full cacheline to L1 for writing in streaming mode */
static inline void prefetch_l1ws_cacheline(void *addr)
{
	/* prefetch cacheline to L1 cache for store */
	__asm__ volatile ("prfm pstl1strm, [%0]" : : "r"(addr) : "memory");
}

BENCH_CACHELINE(prefetch_l1ws_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__ || defined __x86_64__

/* prefetch a full cacheline to L2 for reading */
static inline void prefetch_l2_cacheline(void *addr)
{
#if defined __aarch64__
	/* prefetch cacheline to L2 cache */
	__asm__ volatile ("prfm pldl2keep, [%0]" : : "r"(addr) : "memory");
#else
	/* prefetch cacheline to L2/L3 cache */
	__asm__ volatile ("prefetcht1 %0" : : "m"(*(char*)addr) : "memory");
#endif
}

BENCH_CACHELINE(prefetch_l2_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__

/* prefetch a full cacheline to L2 for writing */
static inline void prefetch_l2w_cacheline(void *addr)
{
	/* prefetch cacheline to L2 cache */
	__asm__ volatile ("prfm pstl2keep, [%0]" : : "r"(addr) : "memory");
}

BENCH_CACHELINE(prefetch_l2w_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__

/* prefetch a full cacheline to L2 for writing in streaming mode */
static inline void prefetch_l2ws_cacheline(void *addr)
{
	/* prefetch cacheline to L2 cache */
	__asm__ volatile ("prfm pstl2strm, [%0]" : : "r"(addr) : "memory");
}

BENCH_CACHELINE(prefetch_l2ws_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__ || defined __x86_64__

/* prefetch a full cacheline to L3 for reading */
static inline void prefetch_l3_cacheline(void *addr)
{
#if defined __aarch64__
	/* prefetch cacheline to L3 cache */
	__asm__ volatile ("prfm pldl3keep, [%0]" : : "r"(addr) : "memory");
#else
	/* prefetch cacheline to L3 cache */
	__asm__ volatile ("prefetcht2 %0" : : "m"(*(char*)addr) : "memory");
#endif
}

BENCH_CACHELINE(prefetch_l3_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__

/* prefetch a full cacheline to L3 for writing */
static inline void prefetch_l3w_cacheline(void *addr)
{
	/* prefetch cacheline to L3 cache */
	__asm__ volatile ("prfm pstl3keep, [%0]" : : "r"(addr) : "memory");
}

BENCH_CACHELINE(prefetch_l3w_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

#if defined __aarch64__

/* prefetch a full cacheline to L3 for writing in streaming mode */
static inline void prefetch_l3ws_cacheline(void *addr)
{
	/* prefetch cacheline to L3 cache */
	__asm__ volatile ("prfm pstl3strm, [%0]" : : "r"(addr) : "memory");
}

BENCH_CACHELINE(prefetch_l3ws_cacheline)

#endif

////////////////////////////////////////////////////////////////////////////////

static void chase_random_prep(void)
{
	prepare_chase_random(map_addr, map_size);
}

static void chase_random_read(void)
{
	struct cacheline *start = (struct cacheline *)map_addr;
	struct cacheline *ptr = start;
	do {
		ptr = ptr->next;
	} while (ptr != start);
}

static void chase_random_write(void)
{
	struct cacheline *start = (struct cacheline *)map_addr;
	struct cacheline *ptr = start;
	do {
		ptr->scratch = 42;
		ptr = ptr->next;
	} while (ptr != start);
}

////////////////////////////////////////////////////////////////////////////////

/* test cases */
static struct test {
	const char *name;
	void (*bench_prep)(void);
	void (*bench_linear)(void);
	void (*bench_step)(size_t);
	const char *desc;
} tests[] = {
	{	.name = "read", BENCH_NAMES(read_cacheline), .desc = "read cacheline",	},
#if defined __aarch64__
	{	.name = "read_ldnp", BENCH_NAMES(ldnp_cacheline), .desc = "read cacheline using LDNP (Arm)",	},
#endif
	{	.name = "write", BENCH_NAMES(write_cacheline), .desc = "write full cacheline (without reading)",	},
#if defined __aarch64__
	{	.name = "write_dczva", BENCH_NAMES(dczva_cacheline), .desc = "write full cacheline using DC ZVA (Arm)",	},
	{	.name = "write_stnp", BENCH_NAMES(stnp_cacheline), .desc = "write full cacheline using STNP (Arm)",	},
#endif
	{	.name = "modify", BENCH_NAMES(modify_cacheline), .desc = "modify cacheline (both read and write)",	},
#if defined __aarch64__
	{	.name = "modify_prefetch", BENCH_NAMES(prefetch_modify_cacheline), .desc = "modify cacheline with prefetching (Arm)",	},
	{	.name = "modify_stnp", BENCH_NAMES(stnp_modify_cacheline), .desc = "modify cacheline using STNP (Arm)",	},
#endif
#if defined __aarch64__ || defined __x86_64__
	{	.name = "prefetch_l1", BENCH_NAMES(prefetch_l1_cacheline), .desc = "prefetch cacheline to L1 for reading",	},
	{	.name = "prefetch_l1w", BENCH_NAMES(prefetch_l1w_cacheline), .desc = "prefetch cacheline to L1 for writing",	},
#endif
#if defined __aarch64__
	{	.name = "prefetch_l1ws", BENCH_NAMES(prefetch_l1ws_cacheline), .desc = "prefetch cacheline to L1 for writing in streaming mode (Arm)",	},
#endif
#if defined __aarch64__ || defined __x86_64__
	{	.name = "prefetch_l2", BENCH_NAMES(prefetch_l2_cacheline), .desc = "prefetch cacheline to L2 for reading",	},
#endif
#if defined __aarch64__
	{	.name = "prefetch_l2w", BENCH_NAMES(prefetch_l2w_cacheline), .desc = "prefetch cacheline to L3 for writing (Arm)",	},
	{	.name = "prefetch_l2ws", BENCH_NAMES(prefetch_l2ws_cacheline), .desc = "prefetch cacheline to L2 for writing in streaming mode (Arm)",	},
#endif
#if defined __aarch64__ || defined __x86_64__
	{	.name = "prefetch_l3", BENCH_NAMES(prefetch_l3_cacheline), .desc = "prefetch cacheline to L3 for reading",	},
#endif
#if defined __aarch64__
	{	.name = "prefetch_l3w", BENCH_NAMES(prefetch_l3w_cacheline), .desc = "prefetch cacheline to L3 for writing (Arm)",	},
	{	.name = "prefetch_l3ws", BENCH_NAMES(prefetch_l3ws_cacheline), .desc = "prefetch cacheline to L3 for writing in streaming mode (Arm)",	},
#endif
	{	.name = "chase_random_r", .bench_prep = chase_random_prep, .bench_linear = chase_random_read, .desc = "random pointer chasing in reading mode",	},
	{	.name = "chase_random_w", .bench_prep = chase_random_prep, .bench_linear = chase_random_write, .desc = "random pointer chasing in writing (modify) mode",	},
	{	.name = NULL, .desc = NULL,	},
};

static void bench_linear(const char *name, void (*bench)(void))
{
	struct timespec ts_now, ts_start, ts_end, ts_tmp;
	unsigned long long bytes_accessed;
	unsigned long long delta_t_ns;
	unsigned long long sum = 0;
	unsigned long long runs;
	int loops;
	double bw;

	printf("linear %s bandwidth over %zu KiB (%zu MiB) block",
	       name, map_size / 1024, (map_size + 1024*1024-1) / 1024 / 1024);
	if (map_huge != 0) {
		printf(" (huge TLB)");
	}
	printf("\n");

	flush_cacheline_all();

	clock_gettime(CLOCK_MONOTONIC, &ts_start);
	ts_end = ts_start;
	timespec_inc_by(&ts_end, option_print_delay_ms * 1000000ull);
	runs = 0;
	loops = 0;

	perf_read(perf_prev_values);

	while (1) {
		bench();
		runs++;

		clock_gettime(CLOCK_MONOTONIC, &ts_now);
		if (!timespec_is_le(&ts_now, &ts_end)) {
			perf_read(perf_curr_values);
			perf_delta(perf_curr_values, perf_prev_values, perf_delta_values);

			bytes_accessed = runs * map_size;
			delta_t_ns = timespec_ns(timespec_sub(&ts_now, &ts_start, &ts_tmp));
			bw = 1000.0 * bytes_accessed / delta_t_ns;

			printf("%.1f MiB/s, %.1f MB/s", to_mib(bw), bw);
			if (num_perf == 1) {
				printf(", perf: %.1f MB/s", 1000.0 * perf_delta_values[0] * perf_factor[0] * CACHELINE_SIZE / delta_t_ns);
				sum = perf_delta_values[0];
			} else if (num_perf >= 2) {
				char delim = ' ';
				sum = 0;

				printf(", perf:");
				for (unsigned int i = 0; i < num_perf; i++) {
					printf("%c%.1f", delim, 1000.0 * perf_delta_values[i] * perf_factor[i] * CACHELINE_SIZE / delta_t_ns);
					sum += perf_delta_values[i] * perf_factor[i];
					delim = '+';
				}
				printf("=%.1f MB/s", 1000.0 * sum * CACHELINE_SIZE / delta_t_ns);
			}
			printf("\n");

			if (csv_file != NULL) {
				fprintf(csv_file, "%s;%d;%llu;%llu;%llu",
				        name, CACHELINE_SIZE, delta_t_ns,
				        bytes_accessed, sum * CACHELINE_SIZE);
				for (unsigned int i = 0; i < num_perf; i++) {
					fprintf(csv_file, ";%llu", perf_delta_values[i]);
				}
				fprintf(csv_file, "\n");
				fflush(csv_file);
			}

			loops++;
			if (loops == option_num_loops) {
				break;
			}

			clock_gettime(CLOCK_MONOTONIC, &ts_start);
			ts_end = ts_start;
			timespec_inc_by(&ts_end, option_print_delay_ms * 1000000ull);
			runs = 0;

			perf_read(perf_prev_values);
		}
	}
}

static void bench_step(const char *name, void (*bench)(size_t), size_t step)
{
	struct timespec ts_now, ts_start, ts_end, ts_tmp;
	unsigned long long bytes_accessed;
	unsigned long long delta_t_ns;
	unsigned long long sum = 0;
	unsigned long long runs;
	int loops;
	double bw;

	printf("step1 %s bandwidth over %zu KiB (%zu MiB) block, step %zu",
	       name, map_size / 1024, (map_size + 1024*1024-1) / 1024 / 1024, step);
	if (map_huge != 0) {
		printf(" (huge TLB)");
	}
	printf("\n");

	flush_cacheline_all();

	clock_gettime(CLOCK_MONOTONIC, &ts_start);
	ts_end = ts_start;
	timespec_inc_by(&ts_end, option_print_delay_ms * 1000000ull);
	runs = 0;
	loops = 0;

	perf_read(perf_prev_values);

	while (1) {
		bench(step);
		runs++;

		clock_gettime(CLOCK_MONOTONIC, &ts_now);
		if (!timespec_is_le(&ts_now, &ts_end)) {
			perf_read(perf_curr_values);
			perf_delta(perf_curr_values, perf_prev_values, perf_delta_values);

			bytes_accessed = runs * map_size * CACHELINE_SIZE / step;
			delta_t_ns = timespec_ns(timespec_sub(&ts_now, &ts_start, &ts_tmp));
			bw = 1000.0 * bytes_accessed / delta_t_ns;

			printf("%.1f MiB/s, %.1f MB/s", to_mib(bw), bw);
			if (num_perf == 1) {
				printf(", perf: %.1f MB/s", 1000.0 * perf_delta_values[0] * perf_factor[0] * CACHELINE_SIZE / delta_t_ns);
				sum = perf_delta_values[0];
			} else if (num_perf >= 2) {
				char delim = ' ';
				sum = 0;

				printf(", perf:");
				for (unsigned int i = 0; i < num_perf; i++) {
					printf("%c%.1f", delim, 1000.0 * perf_delta_values[i] * perf_factor[i] * CACHELINE_SIZE / delta_t_ns);
					sum += perf_delta_values[i] * perf_factor[i];
					delim = '+';
				}
				printf("=%.1f MB/s", 1000.0 * sum * CACHELINE_SIZE / delta_t_ns);
			}
			printf("\n");

			if (csv_file != NULL) {
				fprintf(csv_file, "%s;%zu;%llu;%llu;%llu",
				        name, step, delta_t_ns,
				        bytes_accessed, sum * CACHELINE_SIZE);
				for (unsigned int i = 0; i < num_perf; i++) {
					fprintf(csv_file, ";%llu", perf_delta_values[i]);
				}
				fprintf(csv_file, "\n");
				fflush(csv_file);
			}

			loops++;
			if (loops == option_num_loops) {
				break;
			}

			clock_gettime(CLOCK_MONOTONIC, &ts_start);
			ts_end = ts_start;
			timespec_inc_by(&ts_end, option_print_delay_ms * 1000000ull);
			runs = 0;

			perf_read(perf_prev_values);
		}
	}
}

static void bench_auto(const char *name, void (*bench)(size_t))
{
	struct timespec ts_now, ts_start, ts_end, ts_tmp;
	unsigned long long bytes_accessed;
	unsigned long long delta_t_ns;
	unsigned long long sum = 0;
	unsigned long long runs;
	size_t step, min_step;
	double min_step_bw;
	double bw;

	printf("worst-case %s bandwidth over %zu KiB (%zu MiB) block",
	       name, map_size / 1024, (map_size + 1024*1024-1) / 1024 / 1024);
	if (map_huge != 0) {
		printf(" (huge TLB)");
	}
	printf("\n");

	flush_cacheline_all();

	/* part 1 */

	min_step = -1;
	min_step_bw = 1e30;	/* very high */

	clock_gettime(CLOCK_MONOTONIC, &ts_start);
	ts_end = ts_start;
	timespec_inc_by(&ts_end, option_print_delay_ms * 1000000ull);
	runs = 0;

	perf_read(perf_prev_values);

	step = CACHELINE_SIZE;
	while (1) {
		bench(step);
		runs++;

		clock_gettime(CLOCK_MONOTONIC, &ts_now);
		if (!timespec_is_le(&ts_now, &ts_end)) {
			perf_read(perf_curr_values);
			perf_delta(perf_curr_values, perf_prev_values, perf_delta_values);

			bytes_accessed = runs * map_size * CACHELINE_SIZE / step;
			delta_t_ns = timespec_ns(timespec_sub(&ts_now, &ts_start, &ts_tmp));
			bw = 1000.0 * bytes_accessed / delta_t_ns;

			printf("step %zu: ", step);
			printf("%.1f MiB/s, %.1f MB/s", to_mib(bw), bw);
			if (num_perf == 1) {
				printf(", perf: %.1f MB/s", 1000.0 * perf_delta_values[0] * perf_factor[0] * CACHELINE_SIZE / delta_t_ns);
				sum = perf_delta_values[0];
			} else if (num_perf >= 2) {
				char delim = ' ';
				sum = 0;

				printf(", perf:");
				for (unsigned int i = 0; i < num_perf; i++) {
					printf("%c%.1f", delim, 1000.0 * perf_delta_values[i] * perf_factor[i] * CACHELINE_SIZE / delta_t_ns);
					sum += perf_delta_values[i] * perf_factor[i];
					delim = '+';
				}
				printf("=%.1f MB/s", 1000.0 * sum * CACHELINE_SIZE / delta_t_ns);
			}
			printf("\n");

			if (csv_file != NULL) {
				fprintf(csv_file, "%s;%zu;%llu;%llu;%llu",
				        name, step, delta_t_ns,
				        bytes_accessed, sum * CACHELINE_SIZE);
				for (unsigned int i = 0; i < num_perf; i++) {
					fprintf(csv_file, ";%llu", perf_delta_values[i]);
				}
				fprintf(csv_file, "\n");
				fflush(csv_file);
			}

			if (bw < min_step_bw) {
				min_step_bw = bw;
				min_step = step;
			}
			step *= 2;
			if (step >= map_size / 8) {
				break;
			}

			clock_gettime(CLOCK_MONOTONIC, &ts_start);
			ts_end = ts_start;
			timespec_inc_by(&ts_end, option_print_delay_ms * 1000000ull);
			runs = 0;

			perf_read(perf_prev_values);
		}
	}

	printf("slowest step size: %zu\n", min_step);
}

/* map memory, preferrably using huge TLBs  */
static void *map(size_t size)
{
	char *addr;
	int flags;
	int flags_huge;

	flags = MAP_PRIVATE | MAP_ANONYMOUS;
	flags_huge = MAP_HUGETLB | MAP_HUGE_2MB;
	if (option_huge) {
		flags |= flags_huge;
		map_huge = 1;
	}

again:
	addr = mmap(NULL, size, PROT_READ | PROT_WRITE, flags, -1, 0);
	if (addr == MAP_FAILED) {
		/* if huge TLBs fail, try again with normal pages */
		if ((flags & MAP_HUGETLB) != 0) {
			if (errno == ENOMEM) {
				printf("# mapping memory as huge TLBs failed, this impacts results due to TLB misses\n"
					   "# increase number of huge TLBs: $ sudo sysctl -w vm.nr_hugepages=%zu\n",
					   (size + 0x200000 - 1) / 0x200000);
			}
			flags &= ~flags_huge;
			map_huge = 0;
			goto again;
		}

		perror("mmap");
		exit(EXIT_FAILURE);
	}

	return addr;
}

static void version(FILE *f)
{
	fprintf(f, PROGNAME " build " BUILDID
#ifndef NDEBUG
	        " DEBUG"
#endif
	        "\n");
}

static void usage(FILE *f)
{
	struct test *t;

	version(f);
	fprintf(f, "usage: " PROGNAME " [<options>] <test>\n\n"
	        "Platform memory benchmarks.\n\n"
	        "Options:\n"
	        "  -s|--size <size>  memory size in MiB (default %d MiB on this platform)\n"
	        "  --size-kb <size>  memory size in KiB instead of MiB\n"
	        "  -c|--cpu <cpu>    run on given CPU ID (default any)\n"
	        "  -p|--prio <prio>  run at given priority (default current)\n"
	        "  -l|--loops <num>  stop after given number of loops (default run infinitely)\n"
	        "  -d|--delay <ms>   print bandwidth after given ms (default %d ms)\n"
	        "  --huge            enable huge pages\n"
	        "  --perf            enable perf tracing with default configuration\n"
	        "  --perf-config <pmc[@type[<*|/>factor]][,...]]>  perf configuration\n"
	        "  --step <bytes>    access memory with given step in bytes\n"
	        "  --auto            auto-detect worst-case memory access\n"
	        "  --all             run all tests (don't specify a test, -l 1 set implicitly)\n"
	        "  --csv <file>      export data as CSV to file\n"
	        "  --csv-no-header   do not print a header in the CSV file\n"
	        "  --version         print version info\n"
	        "  --help            show usage\n"
	        "\nPerf configuration examples:\n"
	        "  --perf-config 0x17                    use one PMCs 0x17\n"
	        "  --perf-config 0x17,0x18               use two PMCs 0x17 and 0x18\n"
	        "  --perf-config 0x17,0x18*2             apply a factor of two to the second PMC\n"
	        "  --perf-config 0x19@arm_dsu_pmu*0.25   arm_dsu_pmu PMC 0x19 with factor 0.25\n"
	        "  --perf-config 0x19@arm_dsu_pmu/4      arm_dsu_pmu PMC 0x19 with factor 0.25\n"
	        "\nTests:\n",
	        DEFAULT_MB, option_print_delay_ms);
	for (t = tests; t->name != NULL; t++) {
		fprintf(f, "  %-18s%s\n", t->name, t->desc != NULL ? t->desc : "");
	}
}

int main(int argc, char *argv[])
{
	struct test *t = tests;
	const char *mode_str = NULL;
	const char *size_str = NULL;
	const char *cpu_str = NULL;
	const char *prio_str = NULL;
	const char *step_str = NULL;
	const char *loop_str = NULL;
	const char *delay_str = NULL;
	const char *csv_file_str = NULL;
	const char *perf_config_str = NULL;
	unsigned int step_size = CACHELINE_SIZE;
	unsigned int size = DEFAULT_MB;
	unsigned int size_factor = 1024;	/* MiB mode rather than KiB mode */
	int arg;

	for (arg = 1; arg < argc; arg++) {
		if (argv[arg][0] != '-') {
			if (mode_str != NULL) {
				usage(stderr);
				return EXIT_FAILURE;
			}
			mode_str = argv[arg];
			continue;
		}

		if (!strcmp(argv[arg], "--version")) {
			version(stdout);
			return EXIT_SUCCESS;
		} else if (!strcmp(argv[arg], "--help")) {
			usage(stdout);
			return EXIT_SUCCESS;
		} else if (!strcmp(argv[arg], "--size") || !strcmp(argv[arg], "-s")) {
			arg++;
			size_str = argv[arg];
			size_factor = 1024;
		} else if (!strcmp(argv[arg], "--size-kb")) {
			arg++;
			size_str = argv[arg];
			size_factor = 1;
		} else if (!strcmp(argv[arg], "--cpu") || !strcmp(argv[arg], "-c")) {
			arg++;
			cpu_str = argv[arg];
		} else if (!strcmp(argv[arg], "--prio") || !strcmp(argv[arg], "-p")) {
			arg++;
			prio_str = argv[arg];
		} else if (!strcmp(argv[arg], "--perf")) {
			option_perf = 1;
		} else if (!strcmp(argv[arg], "--perf-config")) {
			option_perf = 1;
			arg++;
			perf_config_str = argv[arg];
		} else if (!strcmp(argv[arg], "--huge")) {
			option_huge = 1;
		} else if (!strcmp(argv[arg], "--step")) {
			option_step = 1;
			arg++;
			step_str = argv[arg];
		} else if (!strcmp(argv[arg], "--loops") || !strcmp(argv[arg], "-l")) {
			arg++;
			loop_str = argv[arg];
		} else if (!strcmp(argv[arg], "--delay") || !strcmp(argv[arg], "-d")) {
			arg++;
			delay_str = argv[arg];
		} else if (!strcmp(argv[arg], "--auto")) {
			option_step = 2;
		} else if (!strcmp(argv[arg], "--all")) {
			option_all = 1;
			/* changes the number of loops to 1 by default */
			option_num_loops = 1;
		} else if (!strcmp(argv[arg], "--csv")) {
			arg++;
			csv_file_str = argv[arg];
		} else if (!strcmp(argv[arg], "--csv-no-header")) {
			option_csv_no_header = 1;
		} else {
			fprintf(stderr, "unknown option '%s'\n", argv[arg]);
			usage(stderr);
			return EXIT_FAILURE;
		}
	}

	if ((arg != argc) || ((option_all == 0) && (mode_str == NULL))
	                  || ((option_all != 0) && (mode_str != NULL))) {
		usage(stderr);
		return EXIT_FAILURE;
	}

	/* check mode */
	if (option_all == 0) {
		for (t = tests; t->name != NULL; t++) {
			if (strcmp(mode_str, t->name) == 0) {
				break;
			}
		}
		if (t->name == NULL) {
			fprintf(stderr, "error: invalid test\n");
			usage(stderr);
			return EXIT_FAILURE;
		}
	}

	/* check CPU */
	if (cpu_str != NULL) {
		cpu_set_t cpuset = { 0 };
		int err;

		pinned_cpu_id = atoi_robust(cpu_str);
		CPU_SET(pinned_cpu_id, &cpuset);
		err = sched_setaffinity(0, sizeof(cpuset), &cpuset);
		if (err != 0) {
			perror("sched_setaffinity");
			exit(EXIT_FAILURE);
		}
	}

	/* check prio */
	if (prio_str != NULL) {
		struct sched_param param = { 0 };
		int prio;
		int err;

		prio = atoi_robust(prio_str);
		param.sched_priority = prio;
		err = sched_setscheduler(0, prio > 0 ? SCHED_FIFO : SCHED_OTHER, &param);
		if (err != 0) {
			perror("sched_setscheduler");
			exit(EXIT_FAILURE);
		}
	}

	/* check size */
	if (size_str != NULL) {
		size = atoi_robust(size_str);
		if (size == 0) {
			fprintf(stderr, "error: invalid memory size in %s\n",
			        (size_factor == 1024) ? "MiB" : "KiB");
			exit(EXIT_FAILURE);
		}
	}

	/* check step size */
	if (step_str != NULL) {
		step_size = atoi_robust(step_str);
		if (step_size < CACHELINE_SIZE || ((step_size & (step_size - 1)) != 0)) {
			fprintf(stderr, "error: invalid step size, must be >=%d and power of two\n", CACHELINE_SIZE);
			exit(EXIT_FAILURE);
		}
	}
	if (step_size >= size * size_factor * 1024) {
		fprintf(stderr, "error: invalid step size, must be < memory size\n");
		exit(EXIT_FAILURE);
	}

	/* check loops */
	if (loop_str != NULL) {
		option_num_loops = atoi_robust(loop_str);
		if (option_num_loops < 0) {
			fprintf(stderr, "error: negative number of loops\n");
			exit(EXIT_FAILURE);
		}
	}

	/* check delay */
	if (delay_str != NULL) {
		option_print_delay_ms = atoi_robust(delay_str);
		if (option_print_delay_ms <= 0) {
			fprintf(stderr, "error: invalid delay\n");
			exit(EXIT_FAILURE);
		}
	}

	map_size = size * size_factor * 1024;
	map_addr = map(map_size);
	map_addr_end = &map_addr[map_size];
	/* prefault memory */
	memset(map_addr, 0x5a, map_size);

	if (option_perf) {
		perf_configure(perf_config_str);
		perf_open();
	}

	/* open CSV file */
	if (csv_file_str != NULL) {
		csv_file = fopen(csv_file_str, "w+");
		if (csv_file == NULL) {
			perror("fopen");
			exit(EXIT_FAILURE);
		}
		if (!option_csv_no_header) {
			fprintf(csv_file, "#test;step;time_in_ns;bytes_accessed;bytes_perf");
			for (unsigned int i = 0; i < num_perf; i++) {
				fprintf(csv_file, ";raw_perf_0x%llx", perf_config[i].config);
				if (perf_config[i].type_name != NULL) {
					fprintf(csv_file, "_%s", perf_config[i].type_name);
				}
			}
			fprintf(csv_file, "\n");
			fflush(csv_file);
		}
	}

	if (option_step == 0) {
		if (option_all != 0) {
			for (t = tests; t->name != NULL; t++) {
				if (t->bench_linear != NULL) {
					if (t->bench_prep != NULL) {
						t->bench_prep();
					}
					bench_linear(t->name, t->bench_linear);
				}
			}
		} else {
			assert(t->name != NULL);
			if (t->bench_linear != NULL) {
				if (t->bench_prep != NULL) {
					t->bench_prep();
				}
				bench_linear(t->name, t->bench_linear);
			}
		}
	} else if (option_step == 1) {
		if (option_all != 0) {
			for (t = tests; t->name != NULL; t++) {
				if (t->bench_step != NULL) {
					if (t->bench_prep != NULL) {
						t->bench_prep();
					}
					bench_step(t->name, t->bench_step, step_size);
				}
			}
		} else {
			assert(t->name != NULL);
			if (t->bench_step != NULL) {
				if (t->bench_prep != NULL) {
					t->bench_prep();
				}
				bench_step(t->name, t->bench_step, step_size);
			}
		}
	} else if (option_step == 2) {
		if (option_all != 0) {
			for (t = tests; t->name != NULL; t++) {
				if (t->bench_step != NULL) {
					if (t->bench_prep != NULL) {
						t->bench_prep();
					}
					bench_auto(t->name, t->bench_step);
				}
			}
		} else {
			assert(t->name != NULL);
			if (t->bench_step != NULL) {
				if (t->bench_prep != NULL) {
					t->bench_prep();
				}
				bench_auto(t->name, t->bench_step);
			}
		}
	}

	if (csv_file != NULL) {
		fclose(csv_file);
	}

	return EXIT_SUCCESS;
}
